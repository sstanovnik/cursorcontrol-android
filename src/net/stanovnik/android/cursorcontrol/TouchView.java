package net.stanovnik.android.cursorcontrol;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * A custom {@link android.view.View} class for handling touch events on it.
 */
public class TouchView extends View {
    public static final String TAG = "TouchView";

    /**
     * The user has this many nanoseconds to perform a click event, otherwise a click is not registered.
     * Related to {@link net.stanovnik.android.cursorcontrol.TouchView#clickEventStartTime}.
     */
    public static final int CLICK_TIMEOUT_NS = 250_000_000;

    /** A representation of the pointer coordinates.  */
    private final MotionEvent.PointerCoords mPointerCoords;

    /** Determines whether we're in a touch chain - the user has begun dragging. */
    private boolean inTouchChain;

    /**
     * Logs the start time of the touch event.
     * Used to prevent a [left/middle/right/double] click from being triggered when
     * the user holds down two fingers in the same spot for a while.
     * Related to {@link net.stanovnik.android.cursorcontrol.TouchView#CLICK_TIMEOUT_NS}.
     */
    private long clickEventStartTime;

    /** Determines whether an ACTION_POINTER_UP will produce a right-click */
    private boolean rightClickEnabled;

    /** Counts the total number of touch events that have occurred since the last {@link android.view.MotionEvent#ACTION_UP}. */
    private long touchEvents;

    /** The previous X coordinate of the touch event. */
    private float previousX;

    /** The previous Y coordinate of the touch event. */
    private float previousY;

    /**
     * Creates a new view and initializes touch processing.
     */
    public TouchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPointerCoords = new MotionEvent.PointerCoords();
        resetClickChain();
    }

    /**
     * Sets the click state parameters to their initial values.
     */
    private void resetClickChain() {
        touchEvents = 0;
        inTouchChain = false;
        rightClickEnabled = false;
        clickEventStartTime = -1;
    }

    /**
     * Checks whether a click request has reached its timeout.
     * @return {@code true} if the event has not yet reached its timeout, false otherwise.
     */
    private boolean clickTimeOK() {
        return (System.nanoTime() - clickEventStartTime) < CLICK_TIMEOUT_NS;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        MessagerThread messager = MessagerThread.getInstance();
        if (messager == null) {
            Log.i(TAG, "Could not obtain a messager. ");
            return true;
        }

        touchEvents++;
        // get the first pointer only
        event.getPointerCoords(0, mPointerCoords);

        // Log.v(TAG, String.format("Action: %s (%f, %f)", MotionEvent.actionToString(event.getAction()), mPointerCoords.x, mPointerCoords.y));
        // Log.v(TAG, String.format("inTouchChain: %b, rightClickEnabled %b, touchEvents: %d, actionIndex: %d", inTouchChain, rightClickEnabled, touchEvents, event.getActionIndex()));

        switch (event.getAction()) {
            // this is the first event that gets fired in a chain of valid touch events
            case MotionEvent.ACTION_DOWN:
                inTouchChain = true;
                clickEventStartTime = System.nanoTime();
                previousX = mPointerCoords.x;
                previousY = mPointerCoords.y;
                break;
            case MotionEvent.ACTION_MOVE:
                // maybe set a lower bound for movement - e.g. 2 pixels minimum to message
                short dx = (short) Math.round(mPointerCoords.x - previousX);
                short dy = (short) Math.round(mPointerCoords.y - previousY);

                messager.sendMessage(MessageScheme.getContents(MessageScheme.TYPE_MOVE, dx, dy));
                // Log.v(TAG, String.format("Moving cursor (%d, %d)", dx, dy));

                // don't allow right clicking if the cursor moved
                if (rightClickEnabled) rightClickEnabled = false;

                previousX = mPointerCoords.x;
                previousY = mPointerCoords.y;
                break;
            case MotionEvent.ACTION_UP:
                // the only two events possible in this case are ACTION_DOWN and ACTION_UP
                if (touchEvents == 2 && clickTimeOK()) {
                    messager.sendMessage(MessageScheme.getContents(MessageScheme.TYPE_CLICK, (short) 0, (short) 0));
                }

                // always end the event chain when the last finger lifts
                resetClickChain();
                break;
            // two cases (in order):
            //  - pressing the first finger when it's been released already
            //  - pressing the second finger while still holding the first one down
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN | 1 << MotionEvent.ACTION_POINTER_INDEX_SHIFT:
                // only start the right click event if we're handling the second touch point
                // and that the only previous event was the first touch point contact
                // otherwise, don't enable (or disable) touch events
                rightClickEnabled = touchEvents == 2 && event.getActionIndex() == 1;
                break;
            // two cases (in order):
            //  - releasing the first finger while still holding the second one down
            //  - releasing the second finger while still holding the first one down
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_POINTER_UP | 1 << MotionEvent.ACTION_POINTER_INDEX_SHIFT:
                if (rightClickEnabled && clickTimeOK()) {
                    messager.sendMessage(MessageScheme.getContents(MessageScheme.TYPE_RIGHTCLICK, (short) 0, (short) 0));
                    rightClickEnabled = false;
                }
                break;
            // special case event handling, reset all values
            case MotionEvent.ACTION_CANCEL:
                resetClickChain();
                break;
        }
        return true;
    }
}
