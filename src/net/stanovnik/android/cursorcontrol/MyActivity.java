package net.stanovnik.android.cursorcontrol;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.net.SocketException;

public class MyActivity extends Activity {
    public static final String TAG = "MyActivity";

    private MessagerThread mMessagerThread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        // load the default preferences if they haven't been loaded yet
        PreferenceManager.setDefaultValues(this, R.xml.preferences, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.preferences:
                startActivity(new Intent(this, Preferences.class));
                break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mMessagerThread = new MessagerThread(this);
            mMessagerThread.start();
        } catch (SocketException e) {
            Log.e(TAG, "Messager thread creation unsuccessful: " + e.getMessage());
        }

        // set the set color in the preferences
        findViewById(R.id.touchView).setBackgroundColor(Color.parseColor(PreferenceManager.getDefaultSharedPreferences(this).getString(Preferences.KEY_TOUCH_COLOUR, "#12345678")));
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mMessagerThread != null) {
            mMessagerThread.stopThread();
        }
    }
}
