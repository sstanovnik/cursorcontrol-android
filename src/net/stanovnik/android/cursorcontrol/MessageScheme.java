package net.stanovnik.android.cursorcontrol;

import android.os.Message;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Defines the structure of a message that is sent to the message queue,
 * as well as the format of the message sent over the wire.
 */
public class MessageScheme {

    /** Defines a static message length */
    public static final int MESSAGE_LENGTH = 5;

    /** Defines 1-byte message type constants. */
    public static final byte TYPE_MOVE       = (byte) 0x0;
    public static final byte TYPE_CLICK      = (byte) 0x1;
    public static final byte TYPE_RIGHTCLICK = (byte) 0x2;
    public static final byte TYPE_ERROR      = (byte) 0xff;

    /** A {@link java.nio.ByteBuffer} is used to efficiently create messages. */
    private static final ByteBuffer bb = ByteBuffer.allocate(MESSAGE_LENGTH).order(ByteOrder.LITTLE_ENDIAN);

    public static Message getContents(byte type, short value1, short value2) {
        Message result = Message.obtain();
        switch (type) {
            case TYPE_CLICK:
                result.arg1 = type;
                result.arg2 = 0;
            case TYPE_MOVE:
                result.arg1 = type;
                result.arg2 = (value2 << 16) + value1;
                break;
            case TYPE_RIGHTCLICK:
                result.arg1 = type;
                result.arg2 = 0;
                break;
            default:
                result.arg1 = TYPE_ERROR;
                result.arg2 = 0;
        }
        return result;
    }

    /**
     * Gets the actual message to be sent over the wire.
     * @param msg The {@link android.os.Message} to be parsed into data.
     * @return A byte array of the encoded message.
     */
    public static byte[] getBytes(Message msg) {
        bb.clear();
        bb.put((byte) msg.arg1).putInt(msg.arg2);
        return bb.array();
    }

}
