package net.stanovnik.android.cursorcontrol;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.net.*;

/**
 * Represents the thread that handles messaging between this device and the desktop client.
 */
public class MessagerThread extends Thread {
    public static final String TAG = "MessagerThread";

    private static MessagerThread INSTANCE;

    /** A socket connected to the desktop client. */
    private final DatagramSocket mSocket;

    /** The context this messager is operating under */
    private final Context mContext;

    /** The {@link android.content.SharedPreferences} object. */
    private final SharedPreferences mPrefs;

    /** A {@link android.os.Handler} for the message queue. */
    private Handler mHandler;

    /**
     * Creates a new {@link MessagerThread}, without starting it.
     * @param parentContext The parent's {@link android.content.Context} object.
     * @throws SocketException if a socket could not be created.
     */
    public MessagerThread(Context parentContext) throws SocketException {
        mContext = parentContext;
        mSocket = new DatagramSocket();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        INSTANCE = this;
    }

    @Override
    public void run() {
        Log.i(TAG, "Starting the looper. ");

        if (!mPrefs.contains(Preferences.KEY_COMPUTER_IP)) {
            Log.e(TAG, "Error accessing the target IP preference. ");
            return;
        }

        final InetAddress address;
        try {
            address = InetAddress.getByName(mPrefs.getString(Preferences.KEY_COMPUTER_IP, "127.0.0.1"));
        } catch (UnknownHostException e) {
            Log.e(TAG, "Error resolving IP/hostname. ");
            return;
        }

        // prepare the looper for the message queue so a handler can be associated with it
        Looper.prepare();

        // create a new handler to handle messages sent to this thread's message queue
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                try {
                    DatagramPacket packet = new DatagramPacket(MessageScheme.getBytes(msg), MessageScheme.MESSAGE_LENGTH, address, 31337);
                    mSocket.send(packet);
                    // Log.v(TAG, "Sent packet: " + Arrays.toString(packet.getData()));
                } catch (UnknownHostException e) {
                    Log.w(TAG, "Unknown host: " + e.getMessage());
                } catch (IOException e) {
                    Log.w(TAG, "I/O exception sending through the socket: " + e.getMessage());
                }
            }
        };

        // start the message queue processing
        Looper.loop();
    }

    /**
     * Add a message to the queue to be sent by the handler.
     * @param msg The message to be sent.
     */
    public void sendMessage(Message msg) {
        if (msg != null) {
            // Log.v(TAG, "Sending message. ");
            mHandler.sendMessage(msg);
        } else {
            Log.i(TAG, "No message to be sent. ");
        }
    }

    /**
     * Stop the thread safely, without processing the rest of the message queue first.
     */
    public void stopThread() {
        INSTANCE = null;
        Looper looper = mHandler.getLooper();
        if (looper != null) {
            Log.i(TAG, "Stopping the looper thread. ");
            looper.quit();
        }
    }

    /**
     * Gets a single existing instance of the messager thread.
     */
    public static MessagerThread getInstance() {
        return INSTANCE;
    }
}
