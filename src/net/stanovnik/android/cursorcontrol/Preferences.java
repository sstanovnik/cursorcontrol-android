package net.stanovnik.android.cursorcontrol;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Preferences extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener {

    public static final String TAG = "Preferences";
    public static final String KEY_COMPUTER_IP = "pref_computerIP";
    public static final String KEY_TOUCH_COLOUR = "pref_touchColour";

    /**
     * Updates the summary of the specified item.
     * @param sharedPreferences The {@link android.content.SharedPreferences} instance.
     * @param key The preference selector string.
     */
    private void updateSummary(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case KEY_COMPUTER_IP:
                findPreference(KEY_COMPUTER_IP).setSummary(sharedPreferences.getString(KEY_COMPUTER_IP, "DEFAULT"));
                break;
            case KEY_TOUCH_COLOUR:
                Preference pref = findPreference(KEY_TOUCH_COLOUR);
                pref.setSummary(((ListPreference) pref).getEntry());
                break;
        }
    }

    /**
     * Called when a specific {@code SharedPreference} changes its value.
     * Processes them after they've already been updated, e.g. changes their summary.
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case KEY_COMPUTER_IP:
                updateSummary(sharedPreferences, key);
                break;
            case KEY_TOUCH_COLOUR:
                updateSummary(sharedPreferences, key);
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        updateSummary(preferences, KEY_COMPUTER_IP);
        updateSummary(preferences, KEY_TOUCH_COLOUR);
    }

    @Override
    public void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        findPreference(KEY_COMPUTER_IP).setOnPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * This is called before a specific {@code Preference} changes. Can be used to reject a change,
     * e.g. checking for formatting.
     * @return Returns {@code true} if the change is accepted and {@code false} if it's rejected.
     */
    @Override
    public boolean onPreferenceChange(Preference preference, final Object newValue) {
        switch (preference.getKey()) {
            case KEY_COMPUTER_IP:
                AsyncTask<Void, Void, InetAddress> task = new AsyncTask<Void, Void, InetAddress>() {
                    @Override
                    protected InetAddress doInBackground(Void[] params) {
                        InetAddress address;
                        try {
                            address = InetAddress.getByName((String) newValue);
                        } catch (UnknownHostException e) {
                            return null;
                        }
                        return address;
                    }
                };

                InetAddress result;
                task.execute();
                try { result = task.get(); }
                catch (Exception e) {
                    Log.w(TAG, "Task execution error");
                    Log.w(TAG, e.getMessage());
                    Toast.makeText(this, R.string.toast_IPparseError, Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (result == null || result.getHostAddress() == null || result.getHostAddress().length() == 0) {
                    Toast.makeText(this, R.string.toast_IPparseError, Toast.LENGTH_SHORT).show();
                    return false;
                }

                break;
        }
        return true;
    }
}
