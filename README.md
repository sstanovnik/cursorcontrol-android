CursorControl-android
==============

An Android counterpart for cursor control. 
Developed for android 4.4. Build for older version at your own risk (it will probably work)

Usage:
------

1. Download, compile and run the Windows counterpart.
2. Install the android app. 
3. Run the app, input your computer's IP address. 
4. Enjoy your new trackpad!
